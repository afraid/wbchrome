window.onload = function() {
    if(document.head.innerHTML.includes("shopify-digital-wallet") || document.body.innerHTML.includes("shopify-digital-wallet")) {
        whatInjection();
    }
}

function whatInjection() {
    var ATCButtons = $(`form[action*="/cart/add"] *[name="add"], form[action*="/cart/add"] **[type="submit"]`)
    ATCButtons.each(function(i, button) {

        $(button).after(`<${$(button).is('button') ? 'button' : 'a'} class="${ATCButtons[i].className}" id="itemWB-${i}">Buy Item WB</${$(button).is('button') ? 'button' : 'a'}>`);
        $(`#itemWB-${i}`).click(function(event) {
            event.preventDefault();
            window.open(`whatbot://${window.location.hostname}${window.location.pathname}`, '_blank');
        })

        $(button).after(`<${$(button).is('button') ? 'button' : 'a'} class="${ATCButtons[i].className}" id="variantWB-${i}">Buy Variant WB</${$(button).is('button') ? 'button' : 'a'}>`);
        $(`#variantWB-${i}`).click(function(event) {
            event.preventDefault();
            let currentVariant = $('*[name="id"]').val();
            if (currentVariant == "" || currentVariant.length <= 1) {
                alert("Size required for [Variant WB]");
            } else {
                window.open(`whatbot://${window.location.hostname}${window.location.pathname}?variant=${currentVariant}`, '_blank');
            }
        })

    })
}